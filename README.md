Assumptions and Notes:
* I didn't explain what a JSON REST API is, how to use it, or what the requirements might be. It's usually described in a separate high-level concept topic. 
* It's likely that there is a maximum limit on the payload size or frequency of use, but that depends on the server implementation. I don't know the specifics, so I just added that it's unlimited :)
* Unrecognized non-key fields are usually ignored with a warning, added a note, and assumed that custom fields exist.
* It's not clear from the draft how the mixed use of `id` and `external_key_field` methods within a single call are handled or prioritized. These are functionally exclusive in most cases, but because I don't know how gracefully the API handles this, I just added that it's not a good idea.
* I used the term "subscriber" instead of "contact" to follow the original Hungarian.
* It wasn't stated explicitly in the draft whether `external_key_field` fields can be any valid non-id field or not. Assumed anything goes.
* I followed the title style convention of other Emarsys docs online, so the gerund form without title capitalization has prevailed.
* Added dummy cross-references in places where I felt it would be justified in a real scenario.
* Assumed "kötegelt" is to be understood as "tömeges", although they are not the exact same.

